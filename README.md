# Katas 🚀 JUMP

## Les Katas pourquoi ?

Les katas sont des exercices de codes, à faire en équipe pour partager, et échanger sur les pratiques de codes dans la bonne humeur (même en période de confinement 😷).

Un kata a un objectif, il met en avant un ou plusieurs concepts de code.
Au travers de notre sélection, nous allons parcourir les concepts de coding suivants :
* Technique de refactoring
* System design
* Test Driven Developpment

Mais nous allons aussi apprendre à mieux utiliser nos outils :
* Intellij
* Framewoks, notamment de les frameworks de test
* Maven & Git

## Notre selection par catégorie

La liste est disponible dans le [wiki](wiki).

Le principe c'est d'avoir le sujet du kata, dans sa version originale sur **master**.

Les solutions seront disponibles sur la branche **solution**, mais si vous êtes ici c'est pour jouer le jeu du training 😎

[wiki]: https://jump-technology.atlassian.net/wiki/spaces/JUMPTECHNO/pages/649527316/Katas

